# Pygame presentation

## Installation

```bash
pip3 install pygame
```
or (on Ubuntu)
```bash
sudo apt install python3-pygame
```

## Goal

We will be attempting to create a game resembling `pygame`'s aliens:

```bash
python3 -m pygame.examples.aliens
```

## Used images
* `res/img/alien.png` is [Unimpressed alien](https://publicdomainvectors.org/en/free-clipart/Unimpressed-alien/55209.html) ([Public domain](https://creativecommons.org/publicdomain/zero/1.0/))

## Used sounds
* `res/sounds/cycles.mp3` is [_Cycles_ by Jason Shaw on Audionautix.com](https://audionautix.com/) ([Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/legalcode))
* `res/sounds/laser.wav` is a sound generated with Python using the `wav` module
