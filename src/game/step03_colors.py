import pygame as pg

pg.init()

TITLE = 'Homemade aliens'

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_DIMENSIONS = (SCREEN_WIDTH, SCREEN_HEIGHT)

FLAGS = 0
VSYNC = 1

pg.display.set_caption(TITLE)
screen = pg.display.set_mode(SCREEN_DIMENSIONS, flags=FLAGS, vsync=VSYNC)

running = True

while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
    screen.fill(pg.Color('magenta'))  # clear the previous frame
    pg.display.update()  # or `flip` (a slight difference in performance)

pg.quit()
