import pygame as pg

pg.init()

TITLE = 'Homemade aliens'

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_DIMENSIONS = (SCREEN_WIDTH, SCREEN_HEIGHT)

FLAGS = 0
VSYNC = 1

BACKGROUND_COLOR = (20, 22, 42)

pg.display.set_caption(TITLE)
screen = pg.display.set_mode(SCREEN_DIMENSIONS, flags=FLAGS, vsync=VSYNC)

running = True

clock = pg.time.Clock()  # Already ticking!

while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
    screen.fill(BACKGROUND_COLOR)
    pg.display.update()
    clock.tick(30)  # or `tick_busy_loop`

pg.quit()
