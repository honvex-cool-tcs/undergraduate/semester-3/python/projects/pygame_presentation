import abc
import random

import pygame as pg

pg.init()

TITLE = 'Homemade aliens'

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_DIMENSIONS = (SCREEN_WIDTH, SCREEN_HEIGHT)

SCREEN_RECT = pg.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)

FLAGS = 0
VSYNC = 1

BACKGROUND_COLOR = (20, 22, 42)


class Entity(abc.ABC):

    @abc.abstractmethod
    def update(self, screen_rect: pg.Rect):
        pass

    @abc.abstractmethod
    def draw(destination: pg.Surface):
        pass


class Alien(Entity):

    IMAGE = pg.image.load('res/img/alien.png')
    IMAGE = pg.transform.smoothscale(
        IMAGE,
        (int(0.15 * IMAGE.get_width()), int(0.15 * IMAGE.get_height()))
    )

    SPEED = 10

    def __init__(self):
        self.rect = Alien.IMAGE.get_rect()
        self.speed = Alien.SPEED
        self.alive = True

    def update(self, screen_rect: pg.Rect):
        self.rect = self.rect.move(self.speed, 0)
        if not screen_rect.contains(self.rect):
            self.rect.move_ip(0, Alien.IMAGE.get_height())
            self.speed *= -1

    def draw(self, destination: pg.Surface):
        destination.blit(Alien.IMAGE, self.rect)

    def check_hits(self, lasers: list):
        if self.rect.collidelist([laser.rect for laser in lasers]) >= 0:
            self.alive = False


class Player(Entity):

    WIDTH = 50
    HEIGHT = 60
    COLOR = pg.Color('darkmagenta')

    SPEED = 4

    def __init__(self):
        self.rect = pg.Rect(
            (SCREEN_WIDTH - Player.WIDTH) // 2,
            SCREEN_HEIGHT - Player.HEIGHT,
            Player.WIDTH,
            Player.HEIGHT
        )
        self.alive = True

    def update(self, _: pg.Rect):
        pass

    def check_hits(self, aliens: list):
        if self.rect.collidelist(aliens) >= 0:  # Returns the index of first hit
            self.alive = False

    def draw(self, destination: pg.Surface):
        pg.draw.rect(destination, Player.COLOR, self.rect)


class Laser(Entity):

    RADIUS = 10
    COLOR = pg.Color('darkolivegreen1')

    SPEED = 10

    def __init__(self, position):
        x, y = position
        self.rect = pg.Rect(
            x - Laser.RADIUS,
            y - Laser.RADIUS,
            2 * Laser.RADIUS,
            2 * Laser.RADIUS
        )

    def update(self, _: pg.Rect):
        self.rect.move_ip(0, -Laser.SPEED)

    def draw(self, destination: pg.Surface):
        pg.draw.circle(
            destination,
            Laser.COLOR,
            self.rect.center,
            Laser.RADIUS
        )


pg.display.set_caption(TITLE)
screen = pg.display.set_mode(SCREEN_DIMENSIONS, flags=FLAGS, vsync=VSYNC)

clock = pg.time.Clock()

running = True

aliens = []
player = Player()
lasers = []

while running and player.alive:
    aliens = [alien for alien in aliens if alien.alive]

    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False

    if random.random() <= 0.1:
        aliens.append(Alien())
    if random.random() <= 0.1:
        lasers.append(Laser(player.rect.center))

    screen.fill(BACKGROUND_COLOR)

    for alien in aliens:
        alien.update(SCREEN_RECT)
        alien.check_hits(lasers)
        alien.draw(screen)
    for laser in lasers:
        laser.update(SCREEN_RECT)
        laser.draw(screen)

    player.update(SCREEN_RECT)
    player.check_hits(aliens)
    player.draw(screen)

    pg.display.update()

    clock.tick(30)

pg.quit()
