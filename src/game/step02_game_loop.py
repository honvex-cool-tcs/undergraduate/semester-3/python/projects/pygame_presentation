import pygame as pg

pg.init()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_DIMENSIONS = (SCREEN_WIDTH, SCREEN_HEIGHT)

FLAGS = pg.RESIZABLE  # `FULSCREEN`, `NOFRAME` and `HIDDEN` are interesting too

pg.display.set_caption('Homemade aliens')
screen = pg.display.set_mode(SCREEN_DIMENSIONS, flags=FLAGS, vsync=1)

running = True

while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False

pg.quit()
