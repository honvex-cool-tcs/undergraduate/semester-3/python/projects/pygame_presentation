import abc
import random

import pygame as pg

pg.init()

TITLE = 'Homemade aliens'

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_DIMENSIONS = (SCREEN_WIDTH, SCREEN_HEIGHT)

SCREEN_RECT = pg.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)

FLAGS = 0
VSYNC = 1

BACKGROUND_COLOR = (20, 22, 42)


class Entity(abc.ABC):

    @abc.abstractmethod
    def update(self, screen_rect: pg.Rect):
        pass

    @abc.abstractmethod
    def draw(destination: pg.Surface):
        pass


class Alien(Entity):

    # Putting it here is an abuse of the order of definitions
    # But let's keep it simple
    IMAGE = pg.image.load('res/img/alien.png')
    IMAGE = pg.transform.smoothscale(
        IMAGE,
        (int(0.15 * IMAGE.get_width()), int(0.15 * IMAGE.get_height()))
    )

    SPEED = 4

    def __init__(self):
        self.rect = Alien.IMAGE.get_rect()
        self.speed = Alien.SPEED

    def update(self, screen_rect: pg.Rect):
        self.rect = self.rect.move(self.speed, 0)
        if not screen_rect.contains(self.rect):
            self.rect.move_ip(0, Alien.IMAGE.get_height())
            self.speed *= -1

    def draw(self, destination: pg.Surface):
        destination.blit(Alien.IMAGE, self.rect)


pg.display.set_caption(TITLE)
screen = pg.display.set_mode(SCREEN_DIMENSIONS, flags=FLAGS, vsync=VSYNC)

running = True

clock = pg.time.Clock()

aliens = []

while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
    if random.random() <= 0.01:
        aliens.append(Alien())
    screen.fill(BACKGROUND_COLOR)
    for alien in aliens:
        alien.update(SCREEN_RECT)
        alien.draw(screen)
    pg.display.update()
    clock.tick(30)

pg.quit()
