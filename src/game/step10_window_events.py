import abc
import random

import pygame as pg

pg.init()

TITLE = 'Homemade aliens'

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_DIMENSIONS = (SCREEN_WIDTH, SCREEN_HEIGHT)

SCREEN_RECT = pg.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)

FLAGS = 0
VSYNC = 1

BACKGROUND_COLOR = (20, 22, 42)
CROSSHAIR_COLOR = pg.Color('red')


class Entity(abc.ABC):

    @abc.abstractmethod
    def update(self, screen_rect: pg.Rect):
        pass

    @abc.abstractmethod
    def draw(destination: pg.Surface):
        pass


class Alien(Entity):

    IMAGE = pg.image.load('res/img/alien.png')
    IMAGE = pg.transform.smoothscale(
        IMAGE,
        (int(0.15 * IMAGE.get_width()), int(0.15 * IMAGE.get_height()))
    )

    SPEED = 10

    def __init__(self):
        self.rect = Alien.IMAGE.get_rect()
        self.speed = Alien.SPEED
        self.alive = True

    def update(self, screen_rect: pg.Rect):
        self.rect = self.rect.move(self.speed, 0)
        if not screen_rect.contains(self.rect):
            self.rect.move_ip(0, Alien.IMAGE.get_height())
            self.speed *= -1

    def draw(self, destination: pg.Surface):
        destination.blit(Alien.IMAGE, self.rect)

    def check_hits(self, lasers: list):
        if self.rect.collidelist([laser.rect for laser in lasers]) >= 0:
            self.alive = False


class Player(Entity):

    WIDTH = 50
    HEIGHT = 60
    COLOR = pg.Color('darkmagenta')

    SPEED = 4

    def __init__(self):
        self.rect = pg.Rect(
            (SCREEN_WIDTH - Player.WIDTH) // 2,
            SCREEN_HEIGHT - Player.HEIGHT,
            Player.WIDTH,
            Player.HEIGHT
        )
        self.alive = True

    def update(self, screen_rect: pg.Rect):
        keys = pg.key.get_pressed()
        if self.rect.left >= 0 and (keys[pg.K_LEFT] or keys[pg.K_a]):
            self.rect.move_ip(-Player.SPEED, 0)
        elif self.rect.right <= screen_rect.right and (keys[pg.K_RIGHT] or keys[pg.K_d]):
            self.rect.move_ip(Player.SPEED, 0)

    def check_hits(self, aliens: list):
        if self.rect.collidelist(aliens) >= 0:
            self.alive = False

    def draw(self, destination: pg.Surface):
        pg.draw.rect(destination, Player.COLOR, self.rect)

    def handle_keydown(self, event: pg.event.Event):
        spawned_lasers = []
        if event.key == pg.K_SPACE:
            spawned_lasers.append(Laser(self.rect.center))
        return spawned_lasers


class Laser(Entity):

    RADIUS = 10
    COLOR = pg.Color('darkolivegreen1')

    SPEED = 10

    def __init__(self, position):
        x, y = position
        self.rect = pg.Rect(
            x - Laser.RADIUS,
            y - Laser.RADIUS,
            2 * Laser.RADIUS,
            2 * Laser.RADIUS
        )

    def update(self, _: pg.Rect):
        self.rect.move_ip(0, -Laser.SPEED)

    def draw(self, destination: pg.Surface):
        pg.draw.circle(
            destination,
            Laser.COLOR,
            self.rect.center,
            Laser.RADIUS
        )


def draw_crosshairs(destination, position):
    pg.draw.circle(destination, CROSSHAIR_COLOR, position, 3)
    pg.draw.circle(destination, CROSSHAIR_COLOR, position, 30, 2)


pg.display.set_caption(TITLE)
screen = pg.display.set_mode(SCREEN_DIMENSIONS, flags=FLAGS, vsync=VSYNC)

pg.mouse.set_visible(False)

clock = pg.time.Clock()

running = True
paused = False

aliens = []
player = Player()
lasers = []

crosshair_position = (0, 0)

while running and player.alive:
    clock.tick(30)

    aliens = [alien for alien in aliens if alien.alive]

    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
        elif event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False
            else:
                lasers.extend(player.handle_keydown(event))
        elif event.type == pg.MOUSEMOTION:
            crosshair_position = event.pos
        elif event.type == pg.WINDOWFOCUSLOST:
            paused = True
        elif event.type == pg.WINDOWFOCUSGAINED:
            paused = False

    if paused:
        continue

    if random.random() <= 0.1:
        aliens.append(Alien())

    screen.fill(BACKGROUND_COLOR)

    for alien in aliens:
        alien.update(SCREEN_RECT)
        alien.check_hits(lasers)
        alien.draw(screen)
    for laser in lasers:
        laser.update(SCREEN_RECT)
        laser.draw(screen)

    player.update(SCREEN_RECT)
    player.check_hits(aliens)
    player.draw(screen)

    draw_crosshairs(screen, crosshair_position)

    pg.display.update()

pg.quit()
