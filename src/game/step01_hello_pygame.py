import pygame as pg  # Let the fun begin

succeeded, failed = pg.init()  # Initialize all pygame subsystems and stuff
print('Successfully initialized:', succeeded)
print('Failed initializations:', failed)

screen = pg.display.set_mode((800, 600))

pg.quit()
